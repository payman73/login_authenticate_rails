class SessionsController < ApplicationController
    def new
    end

    def create
        #render 'new'
        user = User.find_by(email: params[:session][:email].downcase)
        if user && user.authenticate(params[:session][:password])
        flash[:success] = "You have successfully loged in."
        redirect_to user_path(user)
        session[:user_id] = user.id


        else
        flash.now[:danger] = "There was something wrong with your login information. "

        render 'new'


        end



    end

    def destroy
        session[:user_id] = nil
        flash[:success] = "You have loged out."
        redirect_to root_path


    end

end
